package org.szhao.judge.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author zsh
 * 04/May/2020
 */
public class DateTimeUtil {
  public static String formatDateTime(LocalDateTime dateTime) {
    return DateTimeFormatter.ISO_DATE_TIME.format(dateTime);
  }
  public static LocalDateTime parseDateTime(String repr) {
    return LocalDateTime.parse(repr, DateTimeFormatter.ISO_DATE_TIME);
  }
}
