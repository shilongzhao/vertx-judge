package org.szhao.judge.util;

import java.util.Base64;

/**
 * @author zsh
 * 05/May/2020
 */
public class SubmissionUtil {
  public static String encodeBytes(byte[] bytes) {
    return Base64.getEncoder().encodeToString(bytes);
  }

  public static byte[] decodeString(String input) {
    return Base64.getDecoder().decode(input);
  }

}
