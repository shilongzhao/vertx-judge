package org.szhao.judge.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author zsh
 * 28/May/2020
 */
public class SDateTime {
    public LocalDateTime localDateTime;
    public final DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;

}
