package org.szhao.judge.java;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author zsh
 * 29/Apr/2020
 */
public class JavaEvaluationVerticle extends AbstractVerticle {
  public static final String ADDRESS = "java.evaluator";
  private final Logger LOG = LoggerFactory.getLogger(JavaEvaluationVerticle.class);

  @Override
  public void start() {
//    LOG.info(config().encodePrettily());

    vertx.eventBus().consumer(ADDRESS, this::run);
    LOG.info("deploy OK");
  }

  private void run(Message<JsonObject> input) {
    LOG.info("running evaluations against: {}", input.body());
    input.reply("");
  }

  private void preprocess() {
    LOG.info("preprocess");
  }

  private void build() {
    LOG.info("build");
  }

  private void runTests() {
    LOG.info("run tests");
  }

  private void postProcess() {
    LOG.info("post process");
  }
}
