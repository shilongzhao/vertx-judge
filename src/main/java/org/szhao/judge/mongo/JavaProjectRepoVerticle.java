package org.szhao.judge.mongo;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author zsh
 * 07/May/2020
 */
public class JavaProjectRepoVerticle extends AbstractVerticle {
    private final Logger LOG = LoggerFactory.getLogger(JavaProjectRepoVerticle.class);

    public static final String PROJECT_COLLECTION = "projects";
    public static final String ADD_PROJECT = "mongo.project.add";
    public static final String DEL_PROJECT = "mongo.project.del";
    public static final String UPDATE_PROJECT = "mongo.project.update";

    public static final String PROJECT_STUB_COLLECTION = "project_stubs";
    public static final String SAVE_PROJECT_STUB = "mongo.project.stub.save";
    public static final String GET_PROJECT_STUB = "mongo.project.stub.get";

    public static final String PROJECT_TEST_RESOURCES_COLLECTION = "test_resources";
    public static final String SAVE_PROJECT_TEST_RESOURCE = "mongo.project.testResource.save";
    public static final String DEL_PROJECT_TEST_RESOURCE = "mongo.project.testResource.del";

    // TODO: model of project:
    // build command, test command, test resource, build resource, security (access, time limit, memory limit)

    @Override
    public void start() {
        MongoClient client = MongoClient.createShared(vertx, config().getJsonObject("mongo"));
        vertx.eventBus().consumer(ADD_PROJECT, event -> this.createProject(client, event));
        vertx.eventBus().consumer(DEL_PROJECT, message -> this.deleteProject(client, message));
        vertx.eventBus().consumer(UPDATE_PROJECT, message -> this.updateProject(client, message));
    }

    private void updateProject(MongoClient client, Message<Object> message) {
        LOG.info("update project {}", message.body());
        client.updateCollection(PROJECT_COLLECTION, new JsonObject(), new JsonObject(), ar -> {
           message.reply("updated");
        });
    }

    private void deleteProject(MongoClient client, Message<Object> message) {
        LOG.info("delete project {}", message.body());
        client.findOneAndDelete(PROJECT_COLLECTION, new JsonObject(), ar -> {
           message.reply("deleted");
        });
    }

    private void createProject(MongoClient client, Message<Object> message) {
        LOG.info("saved project {}", message.body());
        client.save(PROJECT_COLLECTION, new JsonObject(), ar -> {
            message.reply("created");
        });
    }



}
