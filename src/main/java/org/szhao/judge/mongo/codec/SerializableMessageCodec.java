package org.szhao.judge.mongo.codec;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * @author zsh
 * 12/May/2020
 */
public class SerializableMessageCodec<S extends Serializable>
        implements MessageCodec<S, S> {

    private final Class<S> type;

    public SerializableMessageCodec(Class<S> type) {
        this.type = type;
    }

    @Override
    public void encodeToWire(Buffer buffer, S s) {
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(bos);
            oos.writeObject(s);
            oos.flush();
            buffer.setBytes(0, bos.toByteArray());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public S decodeFromWire(int pos, Buffer buffer) {
        return null;
    }

    @Override
    public S transform(S s) {
        return s;
    }

    @Override
    public String name() {
        return "messageCodec" + type.getCanonicalName();
    }

    @Override
    public byte systemCodecID() {
        return -1;
    }
}
