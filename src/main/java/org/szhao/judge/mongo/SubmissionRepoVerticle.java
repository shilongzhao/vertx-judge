package org.szhao.judge.mongo;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.szhao.judge.util.DateTimeUtil;
import org.szhao.judge.util.SubmissionUtil;

import java.time.LocalDateTime;

/**
 * @author zsh
 * 29/Apr/2020
 */
public class SubmissionRepoVerticle extends AbstractVerticle {
  private final Logger LOG = LoggerFactory.getLogger(SubmissionRepoVerticle.class);

  private static final String UPLOADS = "uploads";
  private static final String SUBMISSIONS = "submissions";

  public static final String CREATE_SUBMISSION = "mongo.submission.create";
  public static final String SAVE_UPLOAD = "mongo.submission.upload.save";
  public static final String GET_UPLOAD = "mongo.submission.upload.get";

  @Override
  public void start() {
//    LOG.info(config().encodePrettily());

    JsonObject mongoConf = config().getJsonObject("mongo");
    MongoClient mongoClient = MongoClient.createShared(vertx, mongoConf);

    vertx.eventBus().<JsonObject>consumer(SAVE_UPLOAD, event -> saveUploadHandler(mongoClient, event));
    vertx.eventBus().<JsonObject>consumer(GET_UPLOAD, event -> getUploadHandler(mongoClient, event));
    vertx.eventBus().<JsonObject>consumer(CREATE_SUBMISSION, event -> createSubmission(mongoClient, event));
    LOG.info("deploy OK");
  }

  private void getUploadHandler(MongoClient client, Message<JsonObject> message) {
    JsonObject query = new JsonObject().put("_id", message.body().getString("id"));
    client.findOne(UPLOADS, query, null, ar -> {
      if (ar.failed()) {
        message.fail(-1, ar.cause().getMessage());
        return;
      }

      JsonObject doc = ar.result();
      if (doc == null) {
        message.fail(-2, "NOT FOUND");
        return;
      }

      LOG.info("found upload file {}:{}:{}",
              doc.getString("_id"), doc.getString("filename"), doc.getString("datetime"));
      message.reply(ar.result());
    });
  }

  private void createSubmission(MongoClient mongoClient, Message<JsonObject> message) {
    mongoClient.save(SUBMISSIONS, message.body(), event -> {
      if (event.succeeded()) message.reply(event.result());
      else {
        LOG.error(event.cause().getMessage());
        message.fail(-1, event.cause().getMessage());
      }
    });
  }

  private void saveUploadHandler(MongoClient client, Message<JsonObject> message) {
    saveUpload(client, message.body()).onComplete(ar -> {
      if (ar.succeeded()) message.reply(ar.result());
      else message.fail(-1, ar.cause().getMessage());
    });
  }

  private Future<String> saveUpload(MongoClient client, JsonObject message) {
    LOG.info("persisting upload file: {}", message);
    Promise<String> saved = Promise.promise();

    String uploadedFilename = message.getString("uploadedFilename");
    if (uploadedFilename == null) {
      saved.fail("no file specified");
      return saved.future();
    }

    vertx.fileSystem().readFile(uploadedFilename, buf -> {
      if (buf.failed()) {
        LOG.info("FAILURE: read file {}", uploadedFilename);
        saved.fail(buf.cause());
        return;
      }
      String data = SubmissionUtil.encodeBytes(buf.result().getBytes());
      JsonObject doc = new JsonObject()
              .put("filename", message.getString("filename"))
              .put("datetime", DateTimeUtil.formatDateTime(LocalDateTime.now()))
              .put("data", data);

      client.insert(UPLOADS, doc, ar -> {
        if (ar.succeeded()) saved.complete(ar.result());
        else {
          LOG.info("FAILURE: save doc.");
          saved.fail(ar.cause());
        }
      });
    });

    return saved.future();
  }

}
