package org.szhao.judge.api;

import io.netty.handler.codec.http.HttpHeaderNames;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.api.contract.openapi3.OpenAPI3RouterFactory;
import io.vertx.ext.web.handler.BodyHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.szhao.judge.mongo.SubmissionRepoVerticle;
import org.szhao.judge.util.DateTimeUtil;
import org.szhao.judge.util.SubmissionUtil;

import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.Set;

import static io.netty.handler.codec.http.HttpResponseStatus.BAD_REQUEST;
import static io.netty.handler.codec.http.HttpResponseStatus.CREATED;
import static io.netty.handler.codec.http.HttpResponseStatus.INTERNAL_SERVER_ERROR;

/**
 * @author zsh
 * 05/May/2020
 */
public class SubmissionAPIVerticle extends AbstractVerticle {
  private final Logger LOG = LoggerFactory.getLogger(SubmissionAPIVerticle.class);

  @Override
  public void start() {
//    LOG.info(config().encodePrettily());
    // read API contract
    OpenAPI3RouterFactory.create(vertx, "api_v1.yaml", ar -> {
      if (ar.failed()) {
        LOG.error("start api verticle KO");
        return;
      }
      OpenAPI3RouterFactory factory = ar.result();
      LOG.info("created router factory OK");

      // handlers
      factory.addHandlerByOperationId("getHomePage", context -> context.response().end("Home"));
      factory.addHandlerByOperationId("uploadFile", this::saveUpload);
      factory.addHandlerByOperationId("createSubmission", this::createSubmission);
      factory.addGlobalHandler(BodyHandler.create().setUploadsDirectory("/tmp/judge-app").setBodyLimit(1024L * 1024L));

      Router router = factory.getRouter();
      // error handlers
      router.errorHandler(400, rx -> {
        JsonObject errorObject = new JsonObject().put("code", 400).put("message",
                rx.failure() != null ? rx.failure().getMessage() : "Validation Exception");
        rx.response().setStatusCode(400).putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .end(errorObject.encode());
      });

      router.errorHandler(INTERNAL_SERVER_ERROR.code(), rx -> {
        JsonObject err = new JsonObject().put("code", 500).put("message", "Internal server error, check with admin");
        rx.response().setStatusCode(500).putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .end(err.encode());
      });

      vertx.createHttpServer()
              .requestHandler(router)
              .listen(config().getInteger("port", 8809), config().getString("host", "localhost"));

    });
//    Router router = Router.router(vertx);
//    // configuration
//    router.route().handler(BodyHandler.create().setUploadsDirectory("/tmp/judge-app").setBodyLimit(1024 * 1024));// 1M limit
//    // paths
//    router.get("/").handler(context -> context.response().end("Home"));
//    router.post("/uploads").handler(this::saveUpload);
//    router.get("/uploads/:id").handler(this::getUpload);
//    router.post("/submissions").handler(this::createSubmission);
//    router.get("/submissions/:id").handler(this::getSubmission);
//
//    vertx.createHttpServer()
//            .requestHandler(router)
//            .listen(config().getInteger("port", 8809), config().getString("host", "localhost"));
  }


  private void getSubmission(RoutingContext context) {

  }


  private void createSubmission(RoutingContext context) {
    JsonObject req = context.getBodyAsJson();
    LOG.info("creating submission for {}", req.encode());
    vertx.eventBus().<JsonObject>request(SubmissionRepoVerticle.CREATE_SUBMISSION, req, ar -> {
      if (ar.succeeded()) {
        context.response().setStatusCode(CREATED.code()).end(ar.result().body().encode());
      } else {
        context.fail(INTERNAL_SERVER_ERROR.code());
      }
    });
  }


  private void getUpload(RoutingContext context) {
    String uploadId = context.pathParam("id");
    JsonObject request = new JsonObject().put("id", uploadId);
    vertx.eventBus().<JsonObject>request(SubmissionRepoVerticle.GET_UPLOAD, request, ar -> {
      if (ar.failed()) {
        LOG.info("failed to get upload id:{}, error = {}", uploadId, ar.cause().getMessage());
        context.response().setStatusCode(BAD_REQUEST.code()).end();
        return;
      }

      JsonObject resp = ar.result().body();
      byte[] bytes = SubmissionUtil.decodeString(resp.getString("data"));
      context.response()
              .putHeader(HttpHeaderNames.CONTENT_TYPE, "application/octet-stream")
              .putHeader(HttpHeaderNames.CONTENT_DISPOSITION, "attachment; " + resp.getString("filename"))
              .end(Buffer.buffer(bytes));
    });
  }

  private void saveUpload(RoutingContext context) {
    // max 1 zip file
    LOG.info(" ========= uploading ========== ");

    Set<FileUpload> fileUploads = context.fileUploads();
    if (fileUploads.size() != 1) {
      LOG.info("NO files found.");
      context.response().setStatusCode(BAD_REQUEST.code()).end();
      return;
    }

    Iterator<FileUpload> iterator = fileUploads.iterator();
    FileUpload zip = iterator.next();

    if (!"application/zip".equals(zip.contentType())) {
      LOG.info("Invalid file type.");
      context.response().setStatusCode(BAD_REQUEST.code()).end();
      return;
    }

    String now = DateTimeUtil.formatDateTime(LocalDateTime.now());

    JsonObject message = new JsonObject().put("datetime", now)
            .put("uploadedFilename", zip.uploadedFileName())
            .put("filename", zip.fileName());
    vertx.eventBus().<String>request(SubmissionRepoVerticle.SAVE_UPLOAD,
            message, ar -> {
              if (ar.failed()) {
                LOG.info("FAILURE: {}", ar.cause().getMessage());
                context.response().setStatusCode(BAD_REQUEST.code()).end("error SAVE_SUBMISSION_UPLOAD");
              } else {
                String id = ar.result().body();
                JsonObject json = new JsonObject().put("id", id);
                context.response().end(json.encode());
                vertx.fileSystem().delete(zip.uploadedFileName(), df -> {
                  LOG.info("delete temp file: {}, result = {}", zip.uploadedFileName(), df.succeeded() ? "OK" : "KO");
                });
              }
            });
  }
}
