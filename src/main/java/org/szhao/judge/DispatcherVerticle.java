package org.szhao.judge;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.szhao.judge.java.JavaEvaluationVerticle;

/**
 * @author zsh
 * 29/Apr/2020
 */
public class DispatcherVerticle extends AbstractVerticle {
  public static final String ADDRESS = "dispatcher";
  private final Logger logger = LoggerFactory.getLogger(DispatcherVerticle.class);
  @Override
  public void start() {
    vertx.eventBus().consumer(ADDRESS, this::dispatch);
    logger.debug("dispatcher deployed");
  }

  private void dispatch(Message<JsonObject> message) {
    logger.debug("dispatching: {}", message.body());
    vertx.eventBus().send(JavaEvaluationVerticle.ADDRESS, message.body());
  }
}
