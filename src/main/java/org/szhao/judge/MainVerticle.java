package org.szhao.judge;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.szhao.judge.api.SubmissionAPIVerticle;
import org.szhao.judge.java.JavaEvaluationVerticle;
import org.szhao.judge.mongo.SubmissionRepoVerticle;

public class MainVerticle extends AbstractVerticle {

  private final Logger LOG = LoggerFactory.getLogger(MainVerticle.class);
  @Override
  public void start() {

    if (config().getJsonObject("mongo") == null) {
      config().put("mongo", new JsonObject());
    }
//    LOG.info(config().encodePrettily());
    DeploymentOptions options = new DeploymentOptions().setConfig(config());

    vertx.deployVerticle(new SubmissionAPIVerticle(), options);
    vertx.deployVerticle(new DispatcherVerticle(), options);
    vertx.deployVerticle(new JavaEvaluationVerticle(), options);
    vertx.deployVerticle(new SubmissionRepoVerticle(), options);

    LOG.info("deploy OK");
  }

}
